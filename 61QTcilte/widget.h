#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_connectBtn_clicked();
    void connectedSlot();
    void disconnectedSlot();
    void recvDataSlot();

private:
    Ui::Widget *ui;
    QTcpSocket socket;
    bool flag;
    QString fileName;
    int fileSize;
    QByteArray arr;
    //char buf[50];
    int i;
};

#endif // WIDGET_H
