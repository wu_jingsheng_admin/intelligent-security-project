#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/videodev2.h> 
#include "tcp.h"
#include "camera.h"


char dataFlow[SIZE * 24] = {0};
unsigned char rgbBuffer[IMAGEHEIGHT * IMAGEWIDTH * 3] = {0};
extern struct VideoBuffer *buffers;

int main()
{
	int sockid = socketInit();
    int connid = accept(sockid, NULL, NULL);
    if(connid < 0)
    {
        perror("connid error");
        close(sockid);
        return -1;
    }
	printf("connid ok\r\n");
	
	//打开设备
	int cameraFd = open(DEFAULT_DEVICE, O_RDWR);
	if(0 > cameraFd)
	{
		perror("cameraFd error");
		close(connid);
		close(sockid);
		return -1;
	}
	
	int ret = 0;
	//设置视频格式
	ret = set_VideoType(cameraFd);
	if(ret < 0)
	{
		close(connid);
		close(sockid);
		close(cameraFd);
		return -1;
	}
	
	//申请帧缓冲 ，分配视频缓冲区
	ret = applyfor_VideoBuffer(cameraFd);
	if(ret < 0)
	{
		close(connid);
		close(sockid);
		close(cameraFd);
		return -1;
	}
	//查询分配的视频缓冲区
	ret = serach_VideoBuffer(cameraFd);
	if(ret < 0)
	{
		close(connid);
		close(sockid);
		close(cameraFd);
		return -1;
	}
	
	//将20个缓冲帧放入队列中并启动数据流
	ret = set_BufferToQueue(cameraFd);
	if(ret < 0)
	{
		close(connid);
		close(sockid);
		close(cameraFd);
		return -1;
	}
	printf("image transmitting......\r\n");
	
	unsigned int index = 0;
	char *jpeg = NULL;  
	unsigned long jpeg_size = 0;
	int len = 0;
	//启动串流
	int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ioctl(cameraFd, VIDIOC_STREAMON, &type);
       
    struct v4l2_buffer buf;
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

        //printf("sleep over\r\n");
	while(1)
	{
//		index = 0;
		//
		ret = dequeue(cameraFd, &index);
		if(ret < 0)
		{
			break;
		}
		//转码压缩
		memset(rgbBuffer, 0, sizeof(IMAGEWIDTH * IMAGEHEIGHT * 3));
		yuyv_to_rgb(buffers[index].start,rgbBuffer);   
		jpeg_size = rgb_to_jpeg(rgbBuffer, (unsigned char **)&jpeg);
		memset(dataFlow, 0, SIZE * 24);
		sprintf(dataFlow, "%ld##", jpeg_size);
		len = strlen(dataFlow);
		memcpy(dataFlow+len, jpeg, jpeg_size);
		ret = send(connid, dataFlow, SIZE * 24, 0);
		if(ret < 0)
		{
			perror("send error:");
			break;
		}
		//入队
                //printf("index = %d\r\n",index);
		//int ret = enqueue(cameraFd,index);
        buf.index = index;
        ret = ioctl(cameraFd,VIDIOC_QBUF,&buf);
		if(ret < 0)
		{
			break;
		}
		free(jpeg);
		jpeg = NULL;
	}
	free(jpeg);
	jpeg = NULL;
	close(connid);
	close(sockid);
	close(cameraFd);
	return 0;
}
