/*===============================================================
 *   Copyright (C) 2020 All rights reserved.
 *   
 *   文件名称：tcp.c
 *   创 建 者：刘世豪
 *   创建日期：2020年10月28日
 *   描    述：
 *
 *   更新日志：
 *
 ================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/epoll.h>

#define IP 192.168.0.224
#define SPORT 8888
#define SIZE 1024

int main(int argc, const char *argv[])
{
    int sockid;
    sockid = socket(AF_INET, SOCK_STREAM, 0);
    if(sockid < 0)
    {
        perror("sockid error");
        return -1;
    }
    printf("sockid ok\r\n");

    int on = 1;
    setsockopt(sockid,SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SPORT);
    addr.sin_addr.s_addr = INADDR_ANY;
    int ret = bind(sockid, (struct sockaddr*)&addr,sizeof(addr));
    if(ret < 0)
    {
        perror("bind error");
        close(sockid);
        return -1;
    }
    printf("bind ok\r\n");

    ret = listen(sockid, 10);
    if(ret < 0)
    {
        perror("listen error");
        close(sockid);
        return -1;
    }
    printf("listen ok\r\n");

    int connid = accept(sockid, NULL, NULL);
    if(connid < 0)
    {
        perror("connid error");
        close(sockid);
        return -1;
    }
    printf("connid ok\r\n");
    int fileSize;
    char fileHead[50] = {0};
    char size[20] = {0};
    struct stat stBuf = {0};
    stat( "./1.png",&stBuf );
    fileSize = stBuf.st_size;
    strcpy( fileHead, "./1.png" );
    strcat( fileHead, "##" );
    sprintf( size, "%d", fileSize );
    strcat( fileHead, size );
    send( connid, fileHead, sizeof(fileHead), 0 );
    int fd = open("./1.png",O_RDONLY);
    if(fd < 0)
    {
        perror("open error");
        close(connid);
        close(sockid);
        return -1;
    }

    char buf[SIZE] = {0};

    /*int epfd = epoll_creat(10);
    if(epfd < 0)
    {
        perror("epoll_creat error");
        close(sockid);
        return -1;
    }
    struct epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.fd = sockid;

    epoll_ctl(epfd,EPOLL_CTL_ADD,sockid,&ev);*/
    while(1)
    {
        memset(buf, 0, sizeof(buf));
        ret = read(fd,buf,sizeof(buf));
        //gets(buf);
        if(ret == 0)
        {
            printf("read over\r\n");
            close(sockid);
            close(connid);
            break;
        }
        ret = send(connid,buf,sizeof(buf),0);
        if(ret < 0)
        {
            perror("recv error");
            close(connid);
            close(sockid);
            return -1;
        }
        //printf("send ok\r\n");

        /*if(0 == strcmp(buf, "quit"))
          {
          break;
          }*/
    }
    close(sockid);
    close(connid);
    close(fd);

    return 0;
}
