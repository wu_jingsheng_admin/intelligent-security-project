#ifndef _TCP_H_
#define _TCP_H_

#define IP "127.0.0.1"
#define SPORT 9999
#define SIZE 1024


/*
 *函数功能：tcp服务器初始化
 *函数名：socketInit
 *函数参数：IP port
 *函数返回值：sockid
 */
int socketInit(void);

/*
 *函数名：get_FileHead
 *函数功能：获取文件头
 *函数参数：
        filePath  --- 文件路径
 *函数返回值：
        fileHead  --- 文件头
 */
char * get_FileHead(char * filePath);
    

/*
 *函数名:sendFile
 *函数功能：发送文件给客户端
 *函数参数：
        filePath --- 文件路径
        sockid --- 套接字描述符
        connid --- 
 *函数返回值:
        成功返回0，失败返回-1
 */
int sendFile(char * filePath, int sockid, int connid, int fd);

#endif